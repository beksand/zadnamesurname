package ZadName;

//Pewna firma planuje wprowadzić nowy produkt na rynek. Chciałaby, żeby reklama tego produktu
//        jak najlepiej była dopasowana do odbiorców. Dlatego też zebrali imiona i nazwiska
//        osób, które kupowały ich produkty. Postanowili w reklamie umieścić najpopularniejsze
//        imiona i nazwiska. Otrzymaliśmy zadanie przygotowania programu, który obliczy
//        ile razy dane imię i nazwisko pojawia się w liście. Przy kopiowaniu wkradły się błędy.
//        Część liter jest z dużej litery. Oczywiście nasz program powinien poradzić sobie z tym.
//        Wyświetlamy dane w formacie (imię i nazwisko z dużej litery - reszta małymi):
//        Imię, ilość wystąpień
//        Imię, ilość wystąpień
//        Imię, ilość wystąpień
//
//        Nazwisko, ilość wystąpień
//        Nazwisko, ilość wystąpień
//
//        maLINOWSka julIA
//        KowAlSka zuZAnna
//        MaliNoWSka maja
//        malINOWska zuZANna
//        NoWAKOWska julIA
//        KowAlSka hANNa
//        NoWAKOWska zOFia
//        KowAlSka lena
//        Nowacka maja
//        WOJTKowska aMelIA
//        MaliNoWSka zofia
//        WojTKOWSka zuZAnnA
//        Nowacka hANna
//        KowAlSka ameLIA
//        WoJTKowska ALIcja
//        KowAlSka ZOfIA
//        Nowakowska maJA
//        MaliNoWSka alEKSAndRA
//        KowAlSka alICJA
//        WojtkoWSKA julIA
//        NoWACka alEKSandra

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        ArrayList<String> nameSurname = new ArrayList<>();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> surname = new ArrayList<>();
        allNameSurname(nameSurname);
        for (int i = 0; i < nameSurname.size(); i++) {
            String[] splitNameSurneme = nameSurname.get(i).toLowerCase().split(" ");
            addNameSurName(splitNameSurneme, name, surname);
            printNameSurname(name.get(i),nameSize(name, i));
            printNameSurname(surname.get(i),nameSize(name, i));
        }

}

    public static void addNameSurName(String[] nameSurname, ArrayList<String> name, ArrayList<String> surname) {

        name.add(nameSurname[0]);
        surname.add(nameSurname[1]);
    }
    public static int nameSize(ArrayList<String> name1, int j) {
        int counter=0;
        for (int i = 0; i < name1.size(); i++) {
            if (name1.get(j).equals(name1.get(i))){
                counter++;
            }
        }
        return counter;
    }
    public static void printNameSurname(String name, int cont){
        System.out.println(name+" ilosc "+cont);
    }
    public static void allNameSurname(ArrayList<String> nameSurname){
        nameSurname.add("maLINOWSka julIA");
        nameSurname.add("KowAlSka zuZAnna");
        nameSurname.add("MaliNoWSka maja");
        nameSurname.add("malINOWska zuZANna");
        nameSurname.add("NoWAKOWska julIA");
        nameSurname.add("KowAlSka hANNa");
        nameSurname.add("NoWAKOWska zOFia");
        nameSurname.add("KowAlSka lena");
        nameSurname.add("Nowacka maja");
        nameSurname.add("WOJTKowska aMelIA");
        nameSurname.add("MaliNoWSka zofia");
        nameSurname.add("WojTKOWSka zuZAnnA");
        nameSurname.add("Nowacka hANna");
        nameSurname.add("KowAlSka ameLIA");
        nameSurname.add("WoJTKowska ALIcja");
        nameSurname.add("KowAlSka ZOfIA");
        nameSurname.add("Nowakowska maJA");
        nameSurname.add("MaliNoWSka alEKSAndRA");
        nameSurname.add("KowAlSka alICJA");
        nameSurname.add("WojtkoWSKA julIA");
        nameSurname.add("NoWACka alEKSandra");


    }
}